# Flush DNS script for any OS

This script is used to flush the DNS (Domain Name System) cache on different operating systems.

The script starts by importing the subprocess and platform modules. subprocess is used to run shell commands, while platform is used to determine the operating system the script is running on.

The flush_dns() function is defined to flush the DNS cache. The function first checks the operating system using the platform.system() method, which returns the name of the operating system. Depending on the operating system, the function assigns a different command to the command variable:

On Windows, the command is ["ipconfig", "/flushdns"], which is used to flush the DNS cache.
On macOS, the command is ["dscacheutil", "-flushcache"], which is used to flush the DNS cache.
On Linux or other Unix-like systems, the command is ["systemd-resolve", "--flush-caches"], which is used to flush the DNS cache.
The script then runs the command using the subprocess.run() method, which runs the command and waits for it to complete. The check=True argument is used to check the return code of the command and raise a CalledProcessError exception if the return code is non-zero. If the command runs successfully, the script prints "DNS cache flushed successfully".

Finally, the function is called at the bottom of script so the flush the DNS cache directly when running the script.